# Source `zdi.sh` on startup.
# Have `zdi help_center -d` running as a daemon
Run a `help_center` shell in the background as a server and send
commands to it.  That way we should be able to re-run RSpec way faster
because we don't suffer the overhead of whatever ZDI and Docker needs
to do to get up and running.
