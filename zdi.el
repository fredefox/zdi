;;; Jira --- A simple wrapper around ZDI

;;; Commentary:

;;; Emacs integration for ZDI.

;;; Code:

(provide 'zdi)
(require 'projectile)

(defun projectile-get-relative-path-buffer ()
  "Get the project-relative path of the current buffer."
  (file-relative-name buffer-file-name (projectile-project-root)))

(defvar zdi-build-command-format "docker exec -it help_center bundle exec rspec --no-color %s")

(defun zdi-set-compile-command ()
  "Set the command used to compile your project to run rspec through zdi."

  (set (make-local-variable 'compile-command)
       (format zdi-build-command-format (projectile-get-relative-path-buffer))))

(defun zdi-find-spec-file ()
  "Go to the spec file corresponding to the currently visited file.

Depends on file naming convention used in Help Center."
  (interactive)
  (find-file (zdi-build-spec-path (buffer-file-name))))

(defun zdi-build-spec-path (path)
  "Guess the location of the spec file corresponding to PATH."
  (replace-regexp-in-string
   "\\(\\)\\.[^\\.]*$" "_spec"
   (replace-regexp-in-string "\\(app\\).*\\'" "spec" path nil nil 1) nil nil 1))

;;; zdi.el ends here
