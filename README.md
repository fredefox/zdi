# Emacs integration for ZDI

A small utility for running rspec directly from the comfort of emacs.
It’s just a first rough version that works for developing on help
center.  I haven’t made it super configurable.  You should modify it
to your own needs or make it more configurable and send a PR.

It allows you to go to an rspec file in help center and run the RSpec
on it by doing `M-x compile`.

## Installation

    (add-to-list 'load-path PATH-TO-ZDI)
    (require zdi)
    (add-hook 'ruby-mode-hook 'zdi-set-compile-command)
